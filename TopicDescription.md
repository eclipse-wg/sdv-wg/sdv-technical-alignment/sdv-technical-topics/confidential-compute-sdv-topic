# Confidential Compute and Multi-tenancy

[Wiki documentation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/confidential-compute-sdv-topic/-/wikis/home) 

# Related SDV projects

None explicit at this time.

# Use cases

-   Execution environment and data authenticity guarantees for 3rd party workloads (e.g. insurance data probe models)

# Cross Initiatives Collaboration 

- Microsoft as sponsor
- Maybe Profian. (good idea; yet they only have x86 while Msft has both x86 and Arm).
- Partnership with Global Semiconductor Alliance automotive group (Microsoft, AWS, NXP, Arm, RAMBus, … - via Francois)
- Confidential Compute through OpenEnclave SDK
    - Thin/hybrid edge with Arm TrustZone
    - Thick edge with Intel SGX until Arm v9
- SSI topic

# Notes:
Close relationship with topic SW orchestration topic.

